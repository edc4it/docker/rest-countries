FROM gradle:4.9-jdk-alpine as builder

COPY --chown=gradle:gradle . /home/gradle/rest-countries
WORKDIR /home/gradle/rest-countries
RUN gradle distTar --no-daemon --console plain

FROM openjdk:8-jdk-alpine
WORKDIR /usr/src/app
COPY --from=builder /home/gradle/rest-countries/build/distributions/rest-countries-1.1.tar /app/
WORKDIR /app
RUN tar -xf rest-countries-1.1.tar
CMD ["rest-countries-1.1/bin/rest-countries"]








