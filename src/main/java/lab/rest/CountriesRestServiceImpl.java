package lab.rest;

import com.fasterxml.jackson.annotation.JsonView;
import lab.rest.impl.dao.CountryRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * todo
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/countries")
public class CountriesRestServiceImpl implements CountryRestService {

    private static final Logger logger = LoggerFactory.getLogger(CountriesRestServiceImpl.class);

    @Autowired
    CountryRepo repo;

    @GetMapping
    @JsonView(CountryViews.Summary.class)
    public List<CountryInfo> getCountries(@RequestParam("continent") String continent) {
        return repo.getCountries(continent);
    }

    @Override
    @PostMapping
    public void addCountry(@RequestBody CountryInfo countryInfo) throws CountryException {
        logger.info("adding country {}",countryInfo);
        repo.addCountry(countryInfo);
    }

    @GetMapping("/{code}")
    public ResponseEntity<Object> getCountry(@PathVariable("code") String code){
        return repo.getCountry(code).map(c->new ResponseEntity(c, HttpStatus.OK)).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }




}