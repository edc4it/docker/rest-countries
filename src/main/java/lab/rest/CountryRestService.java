package lab.rest;

import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * todo
 */
public interface CountryRestService {

    List<CountryInfo>  getCountries(String continent);

    void addCountry(CountryInfo countryInfo) throws CountryException;

    ResponseEntity getCountry(String code);
}
