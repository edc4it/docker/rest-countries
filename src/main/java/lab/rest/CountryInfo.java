package lab.rest;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * todo
 */
public class CountryInfo {
    @JsonView(CountryViews.Summary.class)
    private String code;

    @JsonView(CountryViews.Summary.class)
    private String name;

    private double surfaceArea;
    private int population;
    private String governmentForm;
    private String code2;
    private String continent;


    public CountryInfo(String code, String name, double surfaceArea, int population, String governmentForm, String code2) {
        this.code = code;
        this.name = name;
        this.surfaceArea = surfaceArea;
        this.population = population;
        this.governmentForm = governmentForm;
        this.code2 = code2;

    }

    public CountryInfo(String code, String name, String continent) {
        this.code = code;
        this.name = name;
        this.continent = continent;
    }

    public CountryInfo() {
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(double surfaceArea) {
        this.surfaceArea = surfaceArea;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getGovernmentForm() {
        return governmentForm;
    }

    public void setGovernmentForm(String governmentForm) {
        this.governmentForm = governmentForm;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    @Override
    public String toString() {
        return "Country(" + code +")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CountryInfo)) return false;

        CountryInfo that = (CountryInfo) o;

        return code != null ? code.equals(that.code) : that.code == null;
    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }
}
