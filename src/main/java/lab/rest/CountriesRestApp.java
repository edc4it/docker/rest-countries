package lab.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * todo
 */
@SpringBootApplication
public class CountriesRestApp {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(CountriesRestApp.class, args);
    }
}
