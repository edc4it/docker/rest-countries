package lab.rest.impl.dao;

import lab.rest.CountryException;
import lab.rest.CountryInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * todo
 */
@Repository
public class CountryRepoImpl implements CountryRepo {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public void addCountry(CountryInfo country) throws CountryException {
        if (country == null) throw new IllegalArgumentException("country cannot be null");
        String sql = "INSERT INTO country (code, name, surfacearea, population, governmentform,code2, continent) VALUES (:code,:name,:surfaceArea,:population,:governmentForm,:code2, :continent)";
        try {
            namedParameterJdbcTemplate.update(sql, new BeanPropertySqlParameterSource(country));
        } catch (DuplicateKeyException e) {
            throw new CountryException(country.getCode()+ " already registered");
        }
    }

    @Override
    public List<CountryInfo> getCountries(String continent) {
        if (continent == null) throw new IllegalArgumentException("continent cannot be null");
        String sql = "SELECT code, name, surfacearea as surfaceArea ,  population, continent,  governmentform as  governmentForm, capital, code2 FROM country WHERE LOWER(continent) = ?";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(CountryInfo.class), continent.toLowerCase());
    }

    @Override
    public Optional<CountryInfo> getCountry(String code) {
        if (code == null) throw new IllegalArgumentException("code cannot be null");
        String sql = "SELECT code, name, surfacearea as surfaceArea ,  population, " +
                "continent,  governmentform as  governmentForm, capital, code2 FROM country WHERE code=?";
        List<CountryInfo> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(CountryInfo.class), code);
        if (query.isEmpty())
            return Optional.empty();
        else
            return Optional.of(DataAccessUtils.requiredSingleResult(query));
    }
}
