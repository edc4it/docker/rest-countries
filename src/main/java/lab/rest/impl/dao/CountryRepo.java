package lab.rest.impl.dao;

import lab.rest.CountryException;
import lab.rest.CountryInfo;

import java.util.List;
import java.util.Optional;

/**
 * todo
 */
public interface CountryRepo {
    void addCountry(CountryInfo country) throws CountryException;

    List<CountryInfo> getCountries(String continent);

    Optional<CountryInfo> getCountry(String code);
}
